var AuthenticationModule = angular.module("AuthenticationModule", []);


//routing
AuthenticationModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('authentication', {
            abstract: true,
            url: "/authentication",
            templateUrl: 'modules/authentication/views/index.html',
            controller: 'AuthenticationMainController',
            resolve: {
                lazyload: ['$ocLazyLoad', 'GobalConfig', function($ocLazyLoad, GobalConfig) {
                    return $ocLazyLoad.load({
                        name: 'AuthenticationModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'assets/custom/css/authentication/authentication.css',
                            'modules/authentication/js/controllers/AuthenticationMainController.js'
                        ],
                        cache: GobalConfig.shouldCache
                    });
                }]
            }
        })
        .state('authentication.login', {
            url: "/login",
            templateUrl: 'modules/authentication/views/login.html',
            controller: 'AuthenticationLoginController',
            resolve: {
                lazyload: ['$ocLazyLoad', 'GobalConfig', function($ocLazyLoad, GobalConfig) {
                    return $ocLazyLoad.load({
                        name: 'AuthenticationModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'modules/authentication/js/controllers/AuthenticationLoginController.js'

                        ],
                        cache: GobalConfig.shouldCache
                    });
                }]
            }
        });
});
