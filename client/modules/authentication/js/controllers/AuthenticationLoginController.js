angular.module('AuthenticationModule').controller('AuthenticationLoginController', [
    '$rootScope', '$scope', '$state', '$cookies', 'ChatSocket',
    function($rootScope, $scope, $state, $cookies, ChatSocket) {

        $scope.user = {
            name: ''
        };

        $scope.login = function() {
            if (!ChatSocket.socketConnected) {
                toastr.remove();
                toastr.warning('cannot connect to chat socket, please try again later');
                return;
            }
            if (!$scope.user.name) {
                toastr.remove();
                toastr.warning('please input your name');
                return;
            }
            if ($scope.user.name.length < 6 || $scope.user.name.length > 100) {
                toastr.remove();
                toastr.warning('sorry, name must between 6 to 100 characters, please try again');
                return;
            }
            ChatSocket.authenticationLogin($scope.user.name, '');
        };

        $scope.$on('onAuthenticationLoginFinish', function($event, packet) {
            if (packet.body.code !== 'SUCCESS') {
                toastr.error('something went wrong, please try again later');
                return;
            }
            $cookies.putObject('user', packet.body.data);
            $state.go('app.communication');
        });

        $scope.$on('$viewContentLoaded', function() {});
    }
]);
