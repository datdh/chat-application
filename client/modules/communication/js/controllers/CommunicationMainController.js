angular.module('CommunicationModule').controller('CommunicationMainController', [
    '$rootScope', '$scope', '$cookies', 'ChatSocket',
    function($rootScope, $scope, $cookies, ChatSocket) {

        $scope.loggedUser = $cookies.getObject('user');
        $scope.room = null;
        $scope.messages = [];
        $scope.message = '';

        $scope.initNanoScroller = function(id) {
            $('#' + id).nanoScroller();
        };
        $scope.nanoScrollTo = function(source, target) {
            $('#' + source).nanoScroller({ scrollTo: $('#' + target) });
        };
        $scope.prettyChatList = function() {
            console.log('run');
            setTimeout(function() {
                $scope.initNanoScroller('messages_list');
                $scope.nanoScrollTo('messages_list', 'message_bottom');
            }, 500);
        };

        $scope.sendTextMessage = function() {
            if (!ChatSocket.socketConnected) {
                toastr.remove();
                toastr.warning('cannot connect to chat socket, please try again later');
                return;
            }
            if (!$scope.room) {
                toastr.remove();
                toastr.warning('nothing to send to, please try again later');
                return;
            }
            if (!$scope.message) {
                toastr.remove();
                toastr.warning('nothing to send');
                return;
            }
            if ($scope.message.length > 1000) {
                toastr.remove();
                toastr.warning('sorry, name must be no longer than 1000 characters, please try again');
                return;
            }
            ChatSocket.communicationSendTextMessageRoom($scope.room._id, $scope.message);
            $scope.message = '';
        };

        if (ChatSocket.socketConnected) {
            ChatSocket.authenticationLogin($scope.loggedUser.name, $scope.loggedUser.token);
        }
        $scope.$on('chatSocketConnected', function() {
            ChatSocket.authenticationLogin($scope.loggedUser.name, $scope.loggedUser.token);
        });

        //authentication events
        $scope.$on('onAuthenticationLoginFinish', function($event, packet) {
            if (packet.body.code !== 'SUCCESS') {
                toastr.error('something went wrong, please try again later');
                return;
            }
            ChatSocket.roomJoin('Default');
        });

        //room events
        $scope.$on('onRoomJoinFinish', function($event, packet) {
            if (packet.body.code !== 'SUCCESS') {
                toastr.error('something went wrong, please try again later');
                return;
            }
            if (packet.body.data.user._id === $scope.loggedUser._id) {
                $scope.room = packet.body.data.room;
                ChatSocket.communicationGetMessagesToRoom($scope.room._id);
            }
        });

        //message events
        $scope.$on('onCommunicationGetMessagesToRoomFinish', function($event, packet) {
            if (packet.body.code !== 'SUCCESS') {
                toastr.error('something went wrong, please try again later');
                return;
            }
            $scope.messages = packet.body.data;
            setTimeout(function() {
                $scope.$digest();
            }, 0);
        });
        $scope.$on('onCommunicationSendTextMessageRoomFinish', function($event, packet) {
            if (packet.body.code !== 'SUCCESS') {
                toastr.error('something went wrong, please try again later');
                return;
            }
            $scope.messages.push(packet.body.data);
            setTimeout(function() {
                $scope.$digest();
            }, 0);
        });

        $scope.$on('$viewContentLoaded', function() {});
    }
]);
