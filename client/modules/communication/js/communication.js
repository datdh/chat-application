var CommunicationModule = angular.module("CommunicationModule", []);

//routing
CommunicationModule.config(function($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('app.communication', {
            url: "/communication",
            templateUrl: 'modules/communication/views/index.html',
            controller: 'CommunicationMainController',
            resolve: {
                lazyload: ['$ocLazyLoad', 'GobalConfig', function($ocLazyLoad, GobalConfig) {
                    return $ocLazyLoad.load({
                        name: 'CommunicationModule',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'assets/custom/css/communication/communication.css',
                            'modules/communication/js/controllers/CommunicationMainController.js'
                        ],
                        cache: GobalConfig.shouldCache
                    });
                }]
            }
        });
});
