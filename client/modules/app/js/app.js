var App = angular.module("App", [
    "ui.router",
    "oc.lazyLoad",
    "ngCookies",
    "ngResource",
    "ngSanitize",

    "AuthenticationModule",
    "CommunicationModule"
]);

App.factory('GobalConfig', function() {
    return {
        shouldCache: true,
        chatSocketUrl: ClientConfig.chatSocketUrl
    };
});

//config lazy load
App.config(['$ocLazyLoadProvider', function($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        // ...
    });
}]);

//routing
App.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/authentication/login");

    $stateProvider
        .state('app', {
            abstract: true,
            url: "/app",
            templateUrl: 'modules/app/views/index.html',
            controller: 'AppMainController',
            resolve: {
                lazyload: ['$ocLazyLoad', 'GobalConfig', function($ocLazyLoad, GobalConfig) {
                    return $ocLazyLoad.load({
                        name: 'App',
                        insertBefore: '#ng_load_plugins_before',
                        files: [
                            'modules/app/js/controllers/AppMainController.js'

                        ],
                        cache: GobalConfig.shouldCache
                    });
                }]
            }
        });
});

//socket service
App.factory('ChatSocket', [
    '$rootScope', 'GobalConfig',
    function($rootScope, GobalConfig) {
        var factory = {};


        //params
        factory.socketConnected = false;

        factory.serviceTypes = {
            authentication_login: 'AUTHENTICATION_LOGIN',

            room_join: 'ROOM_JOIN',

            communication_send_text_message_room: 'COMMUNICATION_SENDTEXTMESSAGEROOM',
            communication_get_messages_to_room: 'COMMUNICATION_GETMESSAGESTOROOM'
        };

        //init
        factory.init = function() {

            factory.socketConnected = false;

            factory.socket = io.connect(GobalConfig.chatSocketUrl);

            factory.socket.on('connected', function(packet) {
                factory.socketConnected = true;
                console.log('chat socket connected', packet);
                $rootScope.$broadcast('chatSocketConnected');
            });
            factory.socket.on('server_message', function(packet) {
                console.log('server_message', packet);
            });
            factory.socket.on('server_packet', function(packet) {
                switch (packet.service) {
                    case factory.serviceTypes.authentication_login:
                        factory.onAuthenticationLoginFinish(packet);
                        break;
                    case factory.serviceTypes.room_join:
                        factory.onRoomJoinFinish(packet);
                        break;
                    case factory.serviceTypes.communication_send_text_message_room:
                        factory.onCommunicationSendTextMessageRoomFinish(packet);
                        break;
                    case factory.serviceTypes.communication_get_messages_to_room:
                        factory.onCommunicationGetMessagesToRoomFinish(packet);
                        break;
                    default:
                        console.log('server_packet', packet);
                }
            });
        };
        factory.disconnect = function() {

            factory.socketConnected = false;

            factory.socket.disconnect();
            factory.socket = null;
        };

        //authenticaion
        factory.authenticationLogin = function(name, token) {
            factory.socket.emit('client_packet', { service: factory.serviceTypes.authentication_login, body: { name: name, token: token } });
        };
        factory.onAuthenticationLoginFinish = function(packet) {
            console.log('onAuthenticationLoginFinish', packet);
            $rootScope.$broadcast('onAuthenticationLoginFinish', packet);
        };

        //room
        factory.roomJoin = function(name) {
            factory.socket.emit('client_packet', { service: factory.serviceTypes.room_join, body: { name: name } });
        };
        factory.onRoomJoinFinish = function(packet) {
            console.log('onRoomJoinFinish', packet);
            $rootScope.$broadcast('onRoomJoinFinish', packet);
        };

        //communication
        factory.communicationSendTextMessageRoom = function(room, text) {
            factory.socket.emit('client_packet', { service: factory.serviceTypes.communication_send_text_message_room, body: { room: room, text: text } });
        };
        factory.onCommunicationSendTextMessageRoomFinish = function(packet) {
            console.log('onCommunicationSendTextMessageRoomFinish', packet);
            $rootScope.$broadcast('onCommunicationSendTextMessageRoomFinish', packet);
        };
        factory.communicationGetMessagesToRoom = function(room) {
            factory.socket.emit('client_packet', { service: factory.serviceTypes.communication_get_messages_to_room, body: { room: room } });
        };
        factory.onCommunicationGetMessagesToRoomFinish = function(packet) {
            console.log('onCommunicationGetMessagesToRoomFinish', packet);
            $rootScope.$broadcast('onCommunicationGetMessagesToRoomFinish', packet);
        };

        return factory;
    }
]);

//init run setting
App.run(['$rootScope', '$state', '$cookies', 'GobalConfig', 'ChatSocket', '$cookies',
    function($rootScope, $state, $cookies, GobalConfig, ChatSocket, $cookies) {
        $rootScope.$state = $state; // state to be accessed from view
        $rootScope.GobalConfig = GobalConfig; // GobalConfig to be accessed from view

        //connect chat socket
        ChatSocket.init();

        //ui router change state events
        $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams, options) {
            var loggedUser = $cookies.getObject('user');

            //check authentication
            if (toState.name.indexOf('authentication') === -1) {
                if (!loggedUser || !loggedUser.name || !loggedUser.token) {
                    event.preventDefault();
                    $state.go('authentication.login');
                    return;
                }
            } else {
                if (loggedUser && loggedUser.name && loggedUser.token) {
                    event.preventDefault();
                    $state.go('app.communication');
                    return;
                }
            }
        });

        $rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams) {

        });

        $rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {

        });

        $rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error) {

        });

    }
]);
