var path = require('path');
var async = require('async');
var Room = require(path.join(__dirname, '../', 'schemas/room.js'));

exports.create = function(data, callback) {
    var currentDate = new Date();
    data.created_at = currentDate;
    data.modified_at = currentDate;
    var creatingRoom = new Room(data);
    creatingRoom.save(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};

exports.getWithName = function(data, callback) {
    var options = {
        name: data.name
    };
    var query = Room.findOne(options);
    query.select('_id name created_at modified_at');
    query.exec(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};

exports.getWithId = function(data, callback) {
    var options = {
        _id: data._id
    };
    var query = Room.findOne(options);
    query.select('_id name created_at modified_at');
    query.exec(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};
