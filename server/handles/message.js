var path = require('path');
var async = require('async');
var Message = require(path.join(__dirname, '../', 'schemas/message.js'));

exports.create = function(data, callback) {
    var currentDate = new Date();
    data.created_at = currentDate;
    data.modified_at = currentDate;
    var creatingMessage = new Message(data);
    creatingMessage.save(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};

exports.getWithId = function(data, callback) {
    var options = {
        _id: data._id
    };
    var query = Message.findOne(options);
    query.select('_id from toRoom toUser text created_at modified_at');
    query.populate('from', '_id name');
    query.populate('toRoom', '_id name');
    query.populate('toUser', '_id name');
    query.exec(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};

exports.getMessagesToRoom = function(data, callback) {
    var options = {
        toRoom: data.toRoom
    };
    var query = Message.find(options);
    query.sort({ created_at: -1 });
    query.limit(100);
    query.select('_id from toRoom toUser text created_at modified_at');
    query.populate('from', '_id name');
    query.populate('toRoom', '_id name');
    query.populate('toUser', '_id name');
    query.exec(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};
