var path = require('path');
var async = require('async');
var User = require(path.join(__dirname, '../', 'schemas/user.js'));

exports.create = function(data, callback) {
    var currentDate = new Date();
    data.created_at = currentDate;
    data.modified_at = currentDate;
    var creatingUser = new User(data);
    creatingUser.save(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};

exports.getWithToken = function(data, callback) {
    var options = {
        token: data.token
    };
    var query = User.findOne(options);
    query.select('_id name token role created_at modified_at');
    query.exec(function(error, result) {
        if (error) {
            require(path.join(__dirname, '../', 'ultis/logger.js'))().log('error', JSON.stringify(error));
            if (typeof callback === 'function') return callback(-1, null);
        }
        if (typeof callback === 'function') return callback(null, result);
    });
};
