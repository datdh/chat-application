//components
var path = require('path');
var config = require(path.join(__dirname, '/config.json'));

// set up express
var express = require('express');
var app = express();

//setup mongo
//connect mongodb
var mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(config.mongo_url);
mongoose.connection.on('error', function() {
    require(path.join(__dirname, 'ultis/logger.js'))().log('error', 'MongoDb connection error');
});
mongoose.connection.once('open', function() {
    require(path.join(__dirname, 'ultis/logger.js'))().log('info', 'MongoDb connection success');
});

//setup processors
var serviceType = require(path.join(__dirname, '/constants/serviceType.js'));
var errorType = require(path.join(__dirname, '/constants/errorType.js'));
var authenticationProcessor = require(path.join(__dirname, '/processors/authentication.js'));
var roomProcessor = require(path.join(__dirname, '/processors/room.js'));
var communicationProcessor = require(path.join(__dirname, '/processors/communication.js'));

//params
var usersList = {};
var roomsList = {};

// setup socket
var httpServer = require('http').Server(app);
var io = require('socket.io')(httpServer);
io.on('connection', function(socket) {

    //connection
    socket.emit('connected', {
        message: 'connected to chat server',
        code: errorType.success_code
    });
    socket.on('disconnect', function() {
        delete usersList[socket.userId];
    });

    //client packets
    socket.on('client_packet', function(packet) {
        if (!packet.service || typeof packet.service !== 'string') {
            socket.emit('server_message', {
                message: 'unknown service type',
                code: errorType.error_code
            });
            return;
        }

        switch (packet.service) {
            //authentication service
            case serviceType.authentication_login:
                authenticationProcessor.login(io, socket, packet, usersList);
                break;
                //room service
            case serviceType.room_join:
                roomProcessor.join(io, socket, packet, usersList, roomsList);
                break;
                //communication service
            case serviceType.communication_send_text_message_room:
                communicationProcessor.sendTextMessageRoom(io, socket, packet, usersList);
                break;
            case serviceType.communication_get_messages_to_room:
                communicationProcessor.getMessagesToRoom(io, socket, packet);
                break;
                //unknown
            default:
                socket.emit('server_message', {
                    message: 'unknown service type',
                    code: errorType.error_code
                });
        }
    });
});

// server start
httpServer.listen(config.port, function() {
    console.log('Server start on port: ' + config.port);
    require(path.join(__dirname, 'ultis/logger.js'))().log('info', 'Server start on port: ' + config.port);
});

//create default room
var path = require('path');
var async = require('async');
var roomHandle = require(path.join(__dirname, 'handles/room.js'));
var foundDefaultRoom = null;
async.series({
    checkDefaultRoomExist: function(callback) {
        roomHandle.getWithName({ name: 'Default' }, function(error, result) {
            if (error) {
                return callback(error, null);
            }
            foundDefaultRoom = result;
            return callback(null, null);
        });
    },
    createRoom: function(callback) {
        if (foundDefaultRoom) {
            console.log('Default room already created');
            return callback(null, null);
        }
        roomHandle.create({ name: 'Default' }, function(error, result) {
            if (error) {
                return callback(error, null);
            }
            console.log('Default room created');
            return callback(null, null);
        });
    }
}, function(error, results) {
    if (error) {
        console.log('Create default room error');
    }
});
