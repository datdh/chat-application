var path = require('path');
var async = require('async');

var validator = require(path.join(__dirname, '../', '/ultis/validator.js'));

var serviceType = require(path.join(__dirname, '../', '/constants/serviceType.js'));
var errorType = require(path.join(__dirname, '../', '/constants/errorType.js'));

var roomHandle = require(path.join(__dirname, '../', 'handles/room.js'));

exports.join = function(io, socket, packet, usersList, roomsList) {
    var data = null;
    var foundRoom = null;
    async.series({
        checkPermission: function(callback) {
            if (!socket.userData) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Login required'
                }, null);
            }
            if (socket.userData.role <= 0) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Permission denied'
                }, null);
            }
            return callback(null, null);
        },
        validate: function(callback) {
            var fields = [{
                name: 'name',
                type: 'string',
                required: true,
                bound: [6, 100]
            }];
            validator(packet.body, fields, function(error, result) {
                if (error) {
                    if (error.code === -1) {
                        error.code = errorType.param_required;
                    } else {
                        error.code = errorType.param_invalid;
                    }
                    return callback(error, null);
                }
                data = result;
                return callback(null, null);
            });
        },
        checkRoomExist: function(callback) {
            roomHandle.getWithName({ name: data.name }, function(error, result) {
                if (error) {
                    return callback({
                        code: errorType.mongodb_error,
                        message: 'MongoDB error'
                    }, null);
                }
                if (!result) {
                    return callback({
                        code: errorType.common_not_found,
                        message: 'Room name not found'
                    }, null);
                }
                foundRoom = result.toObject();
                foundRoom._id = foundRoom._id.toHexString();
                return callback(null, null);
            });
        }
    }, function(error, results) {
        if (error) {
            return socket.emit('server_packet', {
                service: serviceType.room_join,
                body: {
                    code: error.code,
                    message: error.message
                }
            });
        }
        socket.join(foundRoom._id);
        if (!roomsList[foundRoom._id]) {
            roomsList[foundRoom._id] = {
                room: foundRoom,
                users: {}
            };
        }
        roomsList[foundRoom._id].users[socket.userData._id] = usersList[socket.userData._id];
        return io.to(foundRoom._id).emit('server_packet', {
            service: serviceType.room_join,
            body: {
                code: errorType.success_code,
                data: {
                    room: foundRoom,
                    users: roomsList[foundRoom._id].users,
                    user: usersList[socket.userData._id]
                }
            }
        });
    });
};
