var path = require('path');
var async = require('async');
var shortid = require('shortid');

var validator = require(path.join(__dirname, '../', '/ultis/validator.js'));

var serviceType = require(path.join(__dirname, '../', '/constants/serviceType.js'));
var errorType = require(path.join(__dirname, '../', '/constants/errorType.js'));

var userHandle = require(path.join(__dirname, '../', 'handles/user.js'));

exports.login = function(io, socket, packet, usersList) {
    var data = null;
    var foundUser = null;
    async.series({
        validate: function(callback) {
            var fields = [{
                name: 'name',
                type: 'string',
                required: true,
                bound: [6, 100]
            }, {
                name: 'token',
                type: 'string',
                required: false,
                bound: [9, 9]
            }];
            validator(packet.body, fields, function(error, result) {
                if (error) {
                    if (error.code === -1) {
                        error.code = errorType.param_required;
                    } else {
                        error.code = errorType.param_invalid;
                    }
                    return callback(error, null);
                }
                data = result;
                return callback(null, null);
            });
        },
        checkUserExist: function(callback) {
            if (data.token) {
                userHandle.getWithToken({ token: data.token }, function(error, result) {
                    if (error) {
                        return callback({
                            code: errorType.mongodb_error,
                            message: 'MongoDB error'
                        }, null);
                    }
                    if (result) {
                        foundUser = result.toObject();
                        foundUser._id = foundUser._id.toHexString();
                    }
                    return callback(null, null);
                });
            } else {
                return callback(null, null);
            }
        },
        login: function(callback) {
            if (foundUser) {
                return callback(null, foundUser)
            } else {
                data.token = shortid.generate();
                userHandle.create({ token: data.token, name: data.name }, function(error, result) {
                    if (error) {
                        return callback({
                            code: errorType.mongodb_error,
                            message: 'MongoDB error'
                        }, null);
                    }
                    foundUser = result.toObject();
                    foundUser._id = foundUser._id.toHexString();
                    return callback(null, null);
                });
            }

        }
    }, function(error, results) {
        if (error) {
            return socket.emit('server_packet', {
                service: serviceType.authentication_login,
                body: {
                    code: error.code,
                    message: error.message
                }
            });
        }
        socket.userData = foundUser;
        usersList[foundUser._id] = {
            _id: foundUser._id,
            name: foundUser.name
        };
        return socket.emit('server_packet', {
            service: serviceType.authentication_login,
            body: {
                code: errorType.success_code,
                data: foundUser
            }
        });
    });
};
