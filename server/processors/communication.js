var path = require('path');
var async = require('async');

var validator = require(path.join(__dirname, '../', '/ultis/validator.js'));

var serviceType = require(path.join(__dirname, '../', '/constants/serviceType.js'));
var errorType = require(path.join(__dirname, '../', '/constants/errorType.js'));

var roomHandle = require(path.join(__dirname, '../', 'handles/room.js'));
var messageHandle = require(path.join(__dirname, '../', 'handles/message.js'));

exports.sendTextMessageRoom = function(io, socket, packet, usersList) {
    var data = null;
    var foundRoom = null;
    var createdMessage = null;
    async.series({
        checkPermission: function(callback) {
            if (!socket.userData) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Login required'
                }, null);
            }
            if (socket.userData.role <= 0) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Permission denied'
                }, null);
            }
            return callback(null, null);
        },
        validate: function(callback) {
            var fields = [{
                name: 'room',
                type: 'string',
                required: true
            }, {
                name: 'text',
                type: 'string',
                required: true,
                bound: [0, 1000]
            }];
            validator(packet.body, fields, function(error, result) {
                if (error) {
                    if (error.code === -1) {
                        error.code = errorType.param_required;
                    } else {
                        error.code = errorType.param_invalid;
                    }
                    return callback(error, null);
                }
                data = result;
                return callback(null, null);
            });
        },
        checkRoomExist: function(callback) {
            roomHandle.getWithId({ _id: data.room }, function(error, result) {
                if (error) {
                    return callback({
                        code: errorType.mongodb_error,
                        message: 'MongoDB error'
                    }, null);
                }
                if (!result) {
                    return callback({
                        code: errorType.common_not_found,
                        message: 'Room id not found'
                    }, null);
                }
                foundRoom = result.toObject();
                foundRoom._id = foundRoom._id.toHexString();
                return callback(null, null);
            });
        },
        createMessage: function(callback) {
            messageHandle.create({
                from: socket.userData._id,
                toRoom: data.room,
                text: data.text
            }, function(error, result) {
                if (error) {
                    return callback({
                        code: errorType.mongodb_error,
                        message: 'MongoDB error'
                    }, null);
                }

                createdMessage = result.toObject();
                createdMessage._id = createdMessage._id.toHexString();
                return callback(null, null);
            });
        }
    }, function(error, results) {
        if (error) {
            return socket.emit('server_packet', {
                service: serviceType.communication_send_text_message_room,
                body: {
                    code: error.code,
                    message: error.message
                }
            });
        }

        return io.to(data.room).emit('server_packet', {
            service: serviceType.communication_send_text_message_room,
            body: {
                code: errorType.success_code,
                data: {
                    from: usersList[socket.userData._id],
                    toRoom: foundRoom,
                    text: data.text
                }
            }
        });
    });
};

exports.getMessagesToRoom = function(io, socket, packet, usersList) {
    var data = null;
    var foundRoom = null;
    var foundMessages = null;
    async.series({
        checkPermission: function(callback) {
            if (!socket.userData) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Login required'
                }, null);
            }
            if (socket.userData.role <= 0) {
                return callback({
                    code: errorType.permission_denied,
                    message: 'Permission denied'
                }, null);
            }
            return callback(null, null);
        },
        validate: function(callback) {
            var fields = [{
                name: 'room',
                type: 'string',
                required: true
            }];
            validator(packet.body, fields, function(error, result) {
                if (error) {
                    if (error.code === -1) {
                        error.code = errorType.param_required;
                    } else {
                        error.code = errorType.param_invalid;
                    }
                    return callback(error, null);
                }
                data = result;
                return callback(null, null);
            });
        },
        checkRoomExist: function(callback) {
            roomHandle.getWithId({ _id: data.room }, function(error, result) {
                if (error) {
                    return callback({
                        code: errorType.mongodb_error,
                        message: 'MongoDB error'
                    }, null);
                }
                if (!result) {
                    return callback({
                        code: errorType.common_not_found,
                        message: 'Room id not found'
                    }, null);
                }
                foundRoom = result.toObject();
                foundRoom._id = foundRoom._id.toHexString();
                return callback(null, null);
            });
        },
        getMesssages: function(callback) {
            messageHandle.getMessagesToRoom({ toRoom: data.room }, function(error, results) {
                if (error) {
                    return callback({
                        code: errorType.mongodb_error,
                        message: 'MongoDB error'
                    }, null);
                }

                foundMessages = results;
                foundMessages.map(function(foundMessage) {
                    foundMessage = foundMessage.toObject();
                    foundMessage._id = foundMessage._id.toHexString();
                });
                return callback(null, null);
            });
        }
    }, function(error, results) {
        if (error) {
            return socket.emit('server_packet', {
                service: serviceType.communication_get_messages_to_room,
                body: {
                    code: error.code,
                    message: error.message
                }
            });
        }

        return socket.emit('server_packet', {
            service: serviceType.communication_get_messages_to_room,
            body: {
                code: errorType.success_code,
                data: foundMessages
            }
        });
    });
};
