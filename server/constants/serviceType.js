//authentication
exports.authentication_login = 'AUTHENTICATION_LOGIN';

//room
exports.room_join = 'ROOM_JOIN';

//communication
exports.communication_send_text_message_room = 'COMMUNICATION_SENDTEXTMESSAGEROOM';
exports.communication_get_messages_to_room = 'COMMUNICATION_GETMESSAGESTOROOM';