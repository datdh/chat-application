//default codes
exports.error_code = 'ERR';
exports.success_code = 'SUCCESS';

//server
exports.server_not_ready = 'ERR_SEVER_NOTREADY';

//param
exports.param_required = 'ERR_PARAM_REQUIRED';
exports.param_invalid = 'ERR_PARAM_INVALID';

//mongo
exports.mongodb_error = 'ERR_MONGO_CONNECTION';

//permission
exports.permission_denied = 'ERR_PERMISSION_DENIED';

//common
exports.common_not_found = 'ERR_COMMON_NOTFOUND';