var path = require('path');

module.exports = function(reqData, fields, callback) {
    var data = {};
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        if (reqData[field.name] && !stringOrNumber(reqData[field.name])) {
            return callback({
                code: -2,
                message: 'The field ' + field.name + ' is not string or number'
            }, null);
        }
        if (field.required && !reqData[field.name]) {
            return callback({
                code: -1,
                message: 'The field ' + field.name + ' is required'
            }, null);
        }
        if (reqData[field.name] && field.type) {
            if (!validType(reqData[field.name], field.type)) {
                return callback({
                    code: -2,
                    message: 'The field ' + field.name + ' is invalid with type: ' + field.type
                }, null);
            }
        }
        if (reqData[field.name] && field.bound && Object.prototype.toString.call(field.bound) === '[object Array]' && field.bound.length === 2 && typeof field.bound[0] === 'number' && typeof field.bound[1] === 'number' && field.bound[0] <= field.bound[1]) {
            if (field.type === 'string' && (reqData[field.name].length < field.bound[0] || reqData[field.name].length > field.bound[1])) {
                return callback({
                    code: -2,
                    message: 'The field ' + field.name + ' is not in range: ' + field.bound[0] + ' to ' + field.bound[1]
                }, null);
            }
            if (field.type === 'number' && (convertData(reqData[field.name], 'number') < field.bound[0] || convertData(reqData[field.name], 'number') > field.bound[1])) {
                return callback({
                    code: -2,
                    message: 'The field ' + field.name + ' is not in range: ' + field.bound[0] + ' to ' + field.bound[1]
                }, null);
            }
        }
        if (reqData[field.name] !== undefined) data[field.name] = convertData(reqData[field.name], field.type);
    };
    return callback(null, data);
};

function stringOrNumber(data) {
    return typeof data === 'string' || typeof data === 'number';
}

function validType(data, type) {
    return typeof data === type;
}

function convertData(data, type) {
    if (type === 'number') {
        if (data === 0 || data) {
            return parseInt(data);
        }
        return undefined;
    } else {
        return data;
    }
}
