var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    from: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true
    },
    toRoom: {
        type: Schema.Types.ObjectId,
        ref: 'Room',
        required: false,
        default: null
    },
    toUser: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: false,
        default: null
    },
    text: {
        type: String,
        required: false,
        default: ''
    },
    created_at: {
        type: Date,
        required: false,
        default: null
    },
    modified_at: {
        type: Date,
        required: false,
        default: null
    }
});

module.exports = mongoose.model('Message', schema);
