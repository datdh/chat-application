var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        required: false,
        default: null
    },
    modified_at: {
        type: Date,
        required: false,
        default: null
    }
});

module.exports = mongoose.model('Room', schema);
