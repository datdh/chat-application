var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    token: {
        type: String,
        required: true,
    },
    role: {
        type: Number,
        required: false,
        default: 1 // o: normal user
    },
    created_at: {
        type: Date,
        required: false,
        default: null
    },
    modified_at: {
        type: Date,
        required: false,
        default: null
    }
});

module.exports = mongoose.model('User', schema);
