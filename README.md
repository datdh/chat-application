# Setup

## Requirement
### Server
- NodeJS: version 4.x or higher
- MongoDB: version 3.x
- Forever: lastest (NodeJS module install globally by command: npm install -g forever)

### Client
- Apache or Nginx for serving

## Config
### Server
Change the config.json file for:
- port: port that server will listen on
- log_path: base path of logs folder (please make sure it's exist)
- mongo_url: connection string for mongodb
### Client
Change the config.js file for:
- chatSocketUrl: url of chat socket server

## Install modules
### Server
In the server folder, run "npm install"
### Client
In the client folder, run "bower install"

## Run server
forever start [path_to_server_folder]/server.js
